	<div class="pre-loader"></div>
	<div class="header clearfix">
		<div class="header-right">
			<div class="brand-logo">
				<a href="index.php">
					<i style="color: green" class="fas fa-feather-alt"> FORMENSCOP </i>
				</a>
			</div>
			<div class="menu-icon">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
			</div>
			<div class="user-info-dropdown">
				<div class="dropdown">
					<!-- menu a gauche : prenom nom et id cachée + menu logout... -->
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon"><i class="fa fa-user-o"></i></span>
						<span class="user-name"><?= $_SESSION['firstName'] ?> <?=  $_SESSION['name'] ?></span>
						<span class="idHide"><?php echo $_SESSION['id'] ?> </span>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a class="dropdown-item" href="profile.php"><i class="fa fa-user-md" aria-hidden="true"></i> Profile</a>
						<a class="dropdown-item" href="profile.php"><i class="fa fa-cog" aria-hidden="true"></i> Setting</a>
						<a class="dropdown-item" href="faq.php"><i class="fa fa-question" aria-hidden="true"></i> Help</a>
						<a class="dropdown-item" href="login.php"><i class="fa fa-sign-out" aria-hidden="true"></i> Log Out</a>
					</div>
				</div>
			</div>
			<!-- NOTIFICATIONS -->
			<div class="user-notification">
				<div class="row">


					<!-- EVENTS NOTIFICATION -->
					<div class="event-notification">
						<div class="dropdown">
							<!-- icone de notification -> menu déroulant des events -->
							<a id="button_event" class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
								<i class="fa fa-bell" aria-hidden="true"></i>
								<!-- modifier la classe de span event en fonction de notif d'évènement -->
								<span id="event" class="badge notification-active"></span>
							</a>
							<!-- contenu du menu déroulant : event non validés en 1er et après les autres par dates -->
							<!-- faire boucle foreach pour afficher les events reçu de la requête  -->
							<div class="dropdown-menu dropdown-menu-right">
								<div class="notification-list mx-h-350 customscroll">
									<ul id="newevent">
										<!-- <li>
											 lien vers le calendrier au jour de l'event
											<a href="#">
												<img src="assets/images/img.jpg" alt="">
												 date event 
												<h3 id="h3notifevent">John Doe <span id="spanevent">3 mins ago</span></h3>
												titre de l'event 
												<p id="pevent">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>

					<!-- MESSAGES NOTIFICATIONS -->
					<div class="message-notification">
						<div class="dropdown">
							<!-- icone de notification -> menu déroulant des messages non lus -->
							<a id="button_message" class="dropdown-toggle no-arrow" href="#" role="button" data-toggle="dropdown">
								<i class="fa fa-envelope" aria-hidden="true"></i>
								<!-- modifier la classe de span event en fonction de notif messages -->
								<span id="message" class="badge notification-active"></span>
							</a>
							<!-- contenu du menu déroulant : event non validés en 1er et après les autres par dates -->
							<!-- faire boucle foreach pour afficher les messages non lus reçus de la requête !// -->
							<!-- prevoir cas si 1 seul msg !! -->
							<div class="dropdown-menu dropdown-menu-right">
								<div class="notification-list mx-h-350 customscroll">
									<ul id="newmsg">
										<!-- affichage des messages non lus -->

										<!-- <li>
											lien vers la conversation dans la messagerie.
											<a href="#">
												<img src="assets/images/img.jpg" alt="">
												expediteur + date
												<h3 id="h3notifmsg">John Doe <span id="spanmsg">3 mins ago</span></h3>
												titre du message
												<p id="pmsg">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed...</p>
											</a>
										</li> -->
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>