	<div class="left-side-bar">
		<div class="brand-logo">
			<a href="/">
			
			<i style ="color: green" class="fas fa-feather-alt"> FORMENSCOP </i>
			
			</a>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					<li>
						<a href="/" class="dropdown-toggle no-arrow">
							<span class="fa fa-home"></span><span class="mtext">Home</span>
						</a>
					</li>
					<li>
						<a href="/emploi" class="dropdown-toggle no-arrow">
							<span class="fa fa-briefcase"></span><span class="mtext">Jobs</span>
						</a>
					</li>
					<li>
						<a href="/calendar" class="dropdown-toggle no-arrow">
							<span class="fa fa-calendar-alt"></span><span class="mtext">Calendar</span>
						</a>
					</li>
					<li>
						<a href="/event" class="dropdown-toggle no-arrow">
							<span class="fa fa-bell"></span><span class="mtext">Event</span>
						</a>
					</li>
					<li>
						<a href="/messagerie" class="dropdown-toggle no-arrow">
							<span class="fa fa-envelope"></span><span class="mtext">Message<span class="fi-burst-new text-danger new"></span></span>
						</a>
					</li>
				
				</ul>
			</div>
		</div>
	</div>