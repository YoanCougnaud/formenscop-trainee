<!DOCTYPE html>
<html>

<head>
	<?php include('include/head.php'); ?>
</head>

<body>

	<?php include('include/header.php'); ?>
	<?php include('include/sidebar.php'); ?>
	<div class="main-container">

		<!-- modale envoi ok-->
		<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
			<div class="modal fade" id="success-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-body text-center font-18">
							<h3 class="mb-20">Message envoyé</h3>
							<div class="mb-30 text-center"><img src="assets/images/success.png"></div>
							Votre message a bien été envoyé.</p>
						</div>
						<div class="modal-footer justify-content-center">
							<button onclick="eraseMsgBar()" type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
						</div>
					</div>
				</div>
			</div>
		
			<!-- modale formulaire incomplet -->
			<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
				<div class="modal fade" id="error-form-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body text-center font-18">
								<h3 class="mb-20">Erreur message</h3>
								<div class="mb-30 text-center"><img src="assets/images/cross.png"></div>
								Formulaire incomplet
							</div>
							<div class="modal-footer justify-content-center">
								<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
							</div>
						</div>
					</div>
				</div>
				<!-- fin modale -->
				<!-- modale pas de destinataire -->
			<div class="pd-20 bg-white border-radius-4 box-shadow mb-30">
				<div class="modal fade" id="no-receiver-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" style="display: none;" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
							<div class="modal-body text-center font-18">
								<h3 class="mb-20">Attention !</h3>
								<div class="mb-30 text-center"><img src="assets/images/cross.png"></div>
								Pas de destinataire sélèctionné.
							</div>
							<div class="modal-footer justify-content-center">
								<button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
							</div>
						</div>
					</div>
				</div>
				<!-- fin modale -->
				<div class="pd-ltr-20 xs-pd-20-10">
					<div class="min-height-200px">
						<div class="page-header">
							<div class="row">
								<div class="col-md-6 col-sm-12">
									<div class="title">
										<h4>Messagerie</h4>
									</div>
									<nav aria-label="breadcrumb" role="navigation">
										<ol class="breadcrumb">
											<li class="breadcrumb-item"><a href="index.php">Home</a></li>
											<li class="breadcrumb-item active" aria-current="page">Messagerie</li>
										</ol>
									</nav>
								</div>
							</div>
						</div>
						<div class="bg-white border-radius-4 box-shadow mb-30">
							<div class="row no-gutters">
								<div class="col-lg-3 col-md-4 col-sm-12">
									<div class="chat-list bg-light-gray">
										<div class="chat-search">
											<span class="ti-search"></span>
											<input type="text" placeholder="Search Contact">
										</div>
										<div class="notification-list chat-notification-list customscroll">
											<ul>
											
															<?php 	
															//var_dump($listRow);

															if( is_array($trainerRow)){
																
																foreach($trainerRow as $key => $value){
																
																	$$key = $value;
																	echo "<li><div id='receiver".$value['Salaried_Account_id']."' onclick='getReceiverId(".$value['Salaried_Account_id'].",\"". $value["FirstName"]."\",\"". $value["Name"]."\")'><h3 class='h3-contact-mess clearfix'>";
																	echo $value["FirstName"]." ".$value["Name"]."<br/><br/>";
																	echo "</h3></div></li>";
															
																}
														} 
												

															if( is_array($coachRow)){
																	
																foreach($coachRow as $key => $value){
																	
																	$$key = $value;
																	echo "<li><div id='receiver".$value['id']."' onclick='getReceiverId(".$value['id'].",\"". $value["FirstName"]."\",\"". $value["Name"]."\")'><h3 class='h3-contact-mess clearfix'>";
																	echo $value["FirstName"]." ".$value["Name"]."<br/><br/>";
																	echo "</h3></div></li>";
															
																}
														} 

																			
															if( is_array($listRow)){
																
																foreach($listRow as $key => $value){
																
																	$$key = $value;
																	echo "<li><div id='receiver".$value['id']."' onclick='getReceiverId(".$value['id'].",\"". $value["FirstName"]."\",\"". $value["Name"]."\")'><h3 class='h3-contact-mess clearfix'>";
																	echo $value["FirstName"]." ".$value["Name"]."<br/><br/>";
																	echo "</h3></div></li>";
															
																}
														} 
												
											
												
														?>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-lg-9 col-md-8 col-sm-12">
									<div class="chat-detail">
										<div class="chat-profile-header clearfix">
											<div class="left">
												<div class="clearfix">



													<div class="chat-profile-name displayList "> Destinataire(s): 
														
													</div>








													<div style="padding-left:15px" class="subject-message chat-profile-name ">
														<input type="text" id="subject-message-input" placeholder="Sujet du message">
													</div>
												</div>
											</div>
											<div class="right text-right">
												<div class="dropdown">
													<a class="btn btn-outline-primary" href="#" role="button" data-toggle="dropdown" onclick="historique()">
														Historique
													</a>
													<a class="btn btn-outline-primary" href="#" role="button" data-toggle="dropdown" onclick="effacerHistorique()">
														Effacer
													</a>
												</div>
											</div>
											
										</div>
										<div class="chat-box">
											<div class="chat-desc customscroll">
												<ul>
													<!---------------- AFFICHAGE DU MESSAGE ENVOYE ------------------>
													<p id="historique">
														<?php
									
														// if (is_array($archives)) {
														// 	foreach ($archives as $key => $value) {
														// 		$$key = $value;
														// 		if ($value['Account_id_sender'] == 9) {
														// 			echo "<li class='clearfix admin_chat' ><div class='chat-body clearfix pull-right'><p style='background-color:#f2d77c'>";
														// 			echo $value['date'] . "<br />" . $value["subject"] . "<br />" . $value['texte'] . "from :" . $value["Account_id_sender"] . "to :" . $value["Account_id_receiver"] . "<br /><br />";
														// 			echo "</p></div></li>";
														// 		} else {
														// 			echo "<li class='clearfix admin_chat' ><div class='chat-body clearfix pull-left'><p style='background-color:#b5c9e8'>";
														// 			echo $value['date'] . "<br />" . $value["subject"] . "<br />" . $value['texte'] . "from :" . $value["Account_id_sender"] . "to :" . $value["Account_id_receiver"] . "<br /><br />";
														// 			echo "</p></div></li><br />";
														// 		}
														// 	}
														// }
														?>
													</p>
													<div class='chat_time'></div>

												</ul>
											</div>
											<div class="chat-footer">
												<div class="chat_text_area">
													<textarea id="message" placeholder="Type your message…"></textarea>
												</div>
												<input type="hidden" id="senderId" value="<?= $senderId ?>">
												<div class="chat_send">
													<button class="btn btn-link" onclick="envoiMessage()" type="submit"><i class="icon-copy ion-paper-airplane"></i></button>
												</div>
												<div id="resultat"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include('include/script.php'); ?>

</body>

</html>