<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;

//Affichage de la vue du Calendar
class CalendarController extends Controller{
    
    public function getAll(){

        $this->render('calendar.php');

    }

}
