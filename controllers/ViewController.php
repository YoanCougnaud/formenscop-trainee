<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;

//controllers relier aux routes demandé (à finir!!)  
class ViewController extends Controller {

    //affiche la page d'accueil
    public function getHome(){

        $this->render('index');

    }

    //affiche la page des ressources (Il faut déterminer la vue à utiliser !!) 
    public function getRessource(){

        // $this->render('');
        echo'ressources';

    }

    //affiche la page des offres d'emploi (Il faut déterminer la vue à utiliser! ) 
    public function getEmploi(){

        // $this->render('');
        echo'Offres d\'emploi';
    }

    //affiche la page des évennements (il faut déterminer la vue à utiliser!)
    public function getEvent(){

        echo'Les évennements';

    }

    //Methode qui affiche la Messagerie
    public function getChat(){

        $this->render('chat');

    }

    // Methode qui permet de logout et de redirectionner sur la page login ( pas opérationnel! à finir!! )
    public function logout(){

        $this->security->deactivate();
        header("Location: http://". $_SERVER['SERVER_NAME' . "/token"]);
        $this->RedirectToRoute('/login');

    }

}
