<?php

namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\controllers\DefaultController;
use BWB\Framework\mvc\dao\DAOMessagerie;
use BWB\Framework\mvc\dao\DAOAccount;
use BWB\Framework\mvc\models\Account;




class MessagerieController extends DefaultController
{
    /**
     * Retourne la vue message
     * @link / methode invoquée lors d'une requete vers les messages
     */
    public function getMessagerie()
    {
       
        $tkn = $this->security->verifyToken($_COOKIE['tkn']);

        $userRow = ( new DAOAccount())->getFullUser($tkn->username);
        $listRow = ( new DAOMessagerie)->getTraineeList();
        $trainerRow = ( new DAOMessagerie)->getTrainer();
        $coachRow = ( new DAOMessagerie)->getCoach();
        $senderId = $userRow->getId();
        
        $array = array("senderId"=>$senderId,"listRow"=>$listRow, "trainerRow"=>$trainerRow,"coachRow"=>$coachRow);
        $this->render("chat",$array);
        
        
    }

   /**
     * Retourne la vue message
     * @link / methode invoquée lors d'une requete vers les messages
     */
    public function getArchivesMessagerie()
    {
        $data = $this->inputPost(); // Recuperation des valeurs postées
        $daoMessagerie = new DAOMessagerie();
        $archives =  $daoMessagerie->getAllBy($data);
        // $archive = array("archives"=>$archives);
        // $tkn = $this->security->verifyToken($_COOKIE['tkn']);
        
        
        
        // $userRow = ( new DAOAccount())->getFullUser($tkn->username);
        // $senderId = $userRow->getId();
        
        // $tblArchives = array("archives"=>$archives, "user"=>$userRow, "senderId"=>$senderId);
        // echo("merde !");die();
        $archives = json_encode($archives); // Encode le tableau en json pour pouvoir être exploité en JS
        print_r($archives);
       
     
    }


    /**
     * Envoi d'un message
     */
    public function sendMessagerie()
    {   
      
        $data = $this->inputPost(); // Recuperation des valeurs postées
        $testMessage = new DAOMessagerie(); // Crée un nouvel objet DAOMessagerie 
        $allReceivers = $data["id_receivers"];
        // La boucle doit se faire ICI =================================================
        
            $testMessage->create($data); // Execute la methode create() de DAOMessagerie
    
        // et se terminer là ==========================================================
    }

    
}
