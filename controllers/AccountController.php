<?php


namespace BWB\Framework\mvc\controllers;

use BWB\Framework\mvc\Controller;
use BWB\Framework\mvc\dao\DAOAccount;



class AccountController extends Controller
{

    protected $_userPost;
    protected $id;
    protected $_user;
    protected $_userId;
    protected $_name;
    protected $_firstname;
    protected $_email;
    protected $_password;
    protected $_roleId;



    /** Envois sur la page login */
    public function getLogin()
    {
        $this->render('login');
    }
   
    /** renvois a la page error 404 sur "/404" */
    public function getError()
    {
        $this->render('404');
    }



    /** Comparaison de l'email et du password avec base de donnée, si ok -> creation du token avec les infos email, password et role*/
    public function getVerifLogin()
    {
        $this->_userPost = $this->inputPost();

        $_userConnexion = new DAOAccount;
        $this->_user = $_userConnexion->getLogin($this->_userPost['email'], $this->_userPost['password']);
        
        if (is_object($this->_user)) {
            
            $this->securityLoader();
            $this->security->generateToken($this->_user);
            $this->_roleId = $this->_user->getRoles();
            
            echo 'true';
        } else {
            echo 'false';
        }
    }



    /** Si token existe, recuperation du role_id dans le token et redirection vers l'application concernée */
    public function userHome()
    {
        

        $tkn = $this->security->verifyToken($_COOKIE['tkn']);

        //print_r($tkn);
        if (!empty($_COOKIE['tkn'])) {

            /**
             * Mettre le "$this->render('index');" sur le bon "cas" et
             * les "header location" dans les autres.
             * 
             * Par exemple si la connection est de GestionRH
             * il faut mettre le "render" dans le cas '1' et
             * mettre les "header location" dans les autres pour rediriger en fonction du role
             * 
             * $this->render('index');
             * 
             * header('Location: http://entreprise.formenscop.bwb');
             * header('Location: http://salarie.formenscop.bwb');
             * header('Location: http://gestionrh.formenscop.bwb');
             * header('Location: http://stagiaire.formenscop.bwb');
             * 
             */

            switch ($tkn->roles) {
                case '1':
                    // redirection vers l'application RH';
                    header('Location: http://gestionrh.formenscop.bwb');
                    break;
                case '2':
                case '3':
                    // affiche la page index.php lorsque les role_id correspondent au role stagiaire
              
                   $userRow = array("user"=>(new DAOAccount())->getFullUser($tkn->username));
                   //print_r($user);
                  $_SESSION['id'] = $userRow['user']->getId();
                  $_SESSION['firstName'] =  $userRow['user']->getFirstName();
                  $_SESSION['name'] =  $userRow['user']->getName();
                 
                   $this->render('index');
                    break;
                case '4':// coach
                case '5':// formateur
                case '6':
                    // redirection vers l'application Salarié';
                    header('Location: http://salarie.formenscop.bwb');
                    break;
                case '7':
                    // redirection vers l'application entreprise partenaire';
                    header('Location: http://entreprise.formenscop.bwb');
                    break;
            }
        } else {

            $this->getError();
        }
    }
}
