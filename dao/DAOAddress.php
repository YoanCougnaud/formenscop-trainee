<?php

namespace BWB\Framework\mvc\dao;

use BWB\Framework\mvc\DAO;
use BWB\Framework\mvc\models\Address;
use PDO;


class DAOAddress extends DAO
{
    public function retrieve($id)
    {
        $query = "SELECT * FROM Address WHERE id=" . $id;
        $result = $this->getPdo()->query($query);
        $result->setFetchMode(PDO::FETCH_ASSOC);
        return $result->fetch();
    }
}
