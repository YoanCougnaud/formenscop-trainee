<?php
namespace BWB\Framework\mvc\dao;

use PDO;
use  BWB\Framework\mvc\DAO;





class DAOMessagerie extends DAO{

    // Lister archives messages avec un contact précis
    public function getAll(){
        // $result = $this->getPdo()->query("SELECT Account_id_sender,Account_id_receiver,subject, texte, date  
        // FROM Message, Communication 
        // WHERE (Message_id = Message.id AND Account_id_sender = 9 AND Account_id_receiver = 5)
        // OR (Message_id = Message.id AND Account_id_sender = 5 AND Account_id_receiver = 9)");
        // $archives = [];
        // while ($row = $result->fetch()){
        //     array_push($archives, $row);
        // }
        // return $archives;
    
    }
    
    public function getAllBy($data){

        // On récupère les données envoyées via la messagerie
        $id_sender = $data["id_sender"];        
        $id_receivers = $data["id_receivers"];
        $archives = [];
        
        foreach($id_receivers as $cle=>$val){
        

            $id_receiver = $val;
        
            
            $result = $this->getPdo()->query("SELECT Account_id_sender,Account_id_receiver, subject, texte, date, Account.FirstName AS firstName, Account.Name AS name 
            FROM Message, Communication, Account 
            WHERE (Message_id = Message.id AND Account.id = Account_id_receiver AND Account_id_sender = $id_sender AND Account_id_receiver = $id_receiver AND Message.removed = '' )
            OR (Message_id = Message.id AND Account.id = Account_id_sender AND Account_id_sender = $id_receiver AND Account_id_receiver = $id_sender AND Message.removed = '') ORDER BY Message.id");
            
            while ($row = $result->fetch()){
                array_push($archives, $row);
            }
        }
            return $archives;
    }



    public function getTraineeList(){ // recupere tous les stagiaires
        $traineeResult = $this->getPdo()->query('SELECT * FROM Account, Trainee WHERE Account_id = Account.id ORDER BY Account.Name');
        $traineeList = [];
        while ($row = $traineeResult->fetch()){
            array_push($traineeList, $row);
        }
        return $traineeList;
    }
    public function getTrainer(){
        $trainerResult = $this->getPdo()->query('SELECT Salaried_Account_id, Account.FirstName, Account.Name
        FROM Trainer, Training, Student, Account
        WHERE Training.id = Trainer.Training_id 
        AND Training.id = Student.Training_id 
        AND Trainer.Salaried_Account_id = Account.id
        AND Student.Trainee_Account_id = 4');
        $trainerList = [];
        while ($row = $trainerResult->fetch()){
            array_push($trainerList, $row);
        }
        return $trainerList;
    }
    public function getCoach(){
        $coachResult = $this->getPdo()->query('SELECT Account.id, Account.FirstName, Account.Name FROM Account 
        WHERE Account.id = (SELECT Accompagniment.Coach_Salaried_Account_id FROM Accompagniment WHERE Trainee_Account_id = 4)');
        $traineeCoach = [];
        while ($row = $coachResult->fetch()){
            array_push($traineeCoach, $row);
        }
        return $traineeCoach;
        var_dump($traineeCoach);
    }

    //Envoi d'un message 
    public function create($data){

        // On récupère les données envoyées via la messagerie
        $texte = $data["texte"]; 
        $subject = $data["subject"];
        $id_sender = $data["id_sender"];
        
        $id_receivers = $data["id_receivers"];

        foreach($id_receivers as $cle=>$val){

            $id_receiver = $val;

            // On rempli la table Message
            $valMessage = ['subject'=>$subject, 'texte'=>$texte];
            $requete = "INSERT INTO Message (subject, texte, removed) VALUES (:subject, :texte, '')";
            $requete_preparee = $this->getPdo()->prepare($requete);
            $requete_preparee->execute($valMessage);
            
            //Récupération de l'ID du dernier message envoyé (foreign key)
            $Message_id = $this->getPdo()->lastInsertId(); 

            // On rempli la table Communication
            $valCommunication = ['id_receiver'=>$id_receiver, 'id_sender'=>$id_sender, 'Message_id'=>$Message_id];
            $requete = "INSERT INTO Communication (Account_id_sender, Account_id_receiver, Message_id) VALUES (:id_sender, :id_receiver, :Message_id)";
            $requete_preparee = $this->getPdo()->prepare($requete);
            $requete_preparee->execute($valCommunication);

            //Récupération de l'ID de la communication
            $Communication_id = $this->getPdo()->lastInsertId();

            // On rempli la table messageState
            $valState = ['Communication_id'=>$Communication_id];
            $requete = "INSERT INTO messageState (Communication_id) VALUES (:Communication_id)";
            
            $requete_preparee = $this->getPdo()->prepare($requete);
            $requete_preparee->execute($valState);
        }

    }
    
    public function retrieve($id){}
        
    public function update($array){}
        
    public function delete($id){}

}