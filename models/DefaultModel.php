<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\UserInterface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of DefaultModel
 *
 * @author loic
 */
class DefaultModel {
    // public function getPassword() {
    //     return "doe";
    // }

    // public function getRoles() {
    //     return [
    //         "admin",
    //         "registered"
    //     ];
    // }

    // public function getUsername() {
    //     return "john";
    // }

    public function parse($array)
    {
        foreach($array as $key => $value){
           $method = 'set'.ucfirst($key);
           if(method_exists($this, $method)){
                $this->$method($value);
           }
           
        }
    }

}
