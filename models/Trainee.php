<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOTrainee;
use BWB\Framework\mvc\models\Account;

class Trainee extends DefaultModel
{
    protected $Account_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOTrainee())->retrieve($id));
        }
    }

    public function setAccount_id($Account_id)
    {
        $this->Account_id = new Account($Account_id);
    }

    public function getAccount_id()
    {
        return $this->Account_id;
    }
}