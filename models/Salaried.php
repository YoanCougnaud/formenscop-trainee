<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOSalaried;
use BWB\Framework\mvc\models\Account;

class Salaried extends DefaultModel
{
    protected $_Account_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){
        $this->parse((new DAOSalaried())->retrieve($id));
        }
    }

    public function setAccount_id($Account_id)
    {
        $this->Account_id = new Account($Account_id);
    }

    public function getAccount_id()
    {
        return $this->Account_id;
    }
}