<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOCompany;
use BWB\Framework\mvc\models\Account;
use BWB\Framework\mvc\models\Coach;

class Company extends DefaultModel
{
    //* Propriétés
    protected $id;
    protected $Account_id;
    protected $Coach_Salaried_Account_id;


    //* Constructeur
    public function __construct($id)
    {
        if(!is_null($id)){

            $this->parse((new DAOCompany)->retrieve($id));
        }
    }


    //* Getters
    public function getId()
    {
        return $this->id;
    }

    public function getAccount_id()
    {
        return $this->Account_id;
    }

    public function getCoach_Salaried_Account_id()
    {
        return $this->Coach_Salaried_Account_id;
    }


    //* Setters

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setAccount_id($Account_id)
    {
        $this->Account_id = new Account($Account_id);
    }

    public function setCoach_Salaried_Account_id($Coach_id)
    {
       $this->Coach_Salaried_Account_id = new Coach($Coach_id);
    }

}

