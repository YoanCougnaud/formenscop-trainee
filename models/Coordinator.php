<?php

namespace BWB\Framework\mvc\models;

use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOCoordinator;
use BWB\Framework\mvc\models\Salaried;

class Coordinator extends DefaultModel
{
    protected $Salaried_Account_id;

    public function __construct($id = null)
    {
        if(!is_null($id)){

            $this->parse((new DAOCoordinator())->retrieve($id));
        }
    }

    public function setSalaried_Account_id($Salaried_Account_id)
    {
        $this->Salaried_Account_id = new Salaried($Salaried_Account_id);
    }

    public function getSalaried_Account_id()
    {
        return $this->Salaried_Account_id;
    }
}