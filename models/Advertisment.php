<?php
namespace BWB\Framework\mvc\models;
use BWB\Framework\mvc\models\DefaultModel;
use BWB\Framework\mvc\dao\DAOAdvertisment;
use BWB\Framework\mvc\models\Company;
use BWB\Framework\mvc\models\Contract;

class Advertisment extends DefaultModel
{
    //* Propriétés
    protected $id;
    protected $date;
    protected $title;
    protected $description;
    protected $Company_id;
    protected $Contract_id;

    //* Constructeur
    public function __construct($id)
    {
        if(!is_null($id)){

            $this->parse((new DAOAdvertisment)->retrieve($id));
        }
    }


    //* Getters

    public function getId()
    {
        return $this->id;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getCompany_id()
    {
        return $this->Company_id;
    }

    public function getContract_id()
    {
        return $this->Contract_id;
    }


    //* Setters

    public function setId($id)
    {
        $this->id = $id;
    }

    public function setDate($date)
    {
       $this->date = $date;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function setDescription($description)
    {
       $this->description = $description;
    }

    public function setCompany_id($Company_id)
    {
       $this->Company_id = new Company($Company_id);
    }

    public function setContract_id($Contract_id)
    {
        $this->Contract_id = new Contract($Contract_id);
    }
}