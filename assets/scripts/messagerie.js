const tblReceivers = []; // Tableau qui récupère l'id des destinataires



// Fonction envoi de message
function envoiMessage() { 
    
    let message = document.querySelector("#message").value; // Recupère le message
    let subject = document.querySelector("#subject-message-input").value; // Recupere le sujet
    let id_sender = document.querySelector("#senderId").value; // ID de l'émetteur
    let id_receivers = tblReceivers; // id du destinataire
    let formError = false;
    // Vérification du formulaire
    if(message === "" || subject === ""){
        
        $("#error-form-modal").modal();
        $("#error-form-modal").show();
        formError = true;
    }

    if (formError !== true){
    // methode ajax pour envoyer les infos à MessagerieController via la route /messagerie/send en post
    $.ajax({
        url: "/messagerie/send",
        type: "post",
        data: {
            subject: subject,
            texte: message,
            id_sender: id_sender,
            id_receivers: id_receivers,
        },
        success: function(data) { // retourne la réponse de MessagerieController
        
        // Déclenchement de la modale de confirmation de l'envoi
        $("#success-modal").modal();
        $("#success-modal").show();


        $("#receiver").html(id_receivers);
        
        // $("#message-archive").html($archives);
        
        
        
        },
        error: function() {
            alert("Erreur Ajax !");
        }
    });

    }
}



// Fonction qui rafraîchit la messagerie 
// au clic sur le bouton de la modale de confirmation d'envoie de message
function redirect(){
    
    window.location.replace("/messagerie");
}



// Fonction qui supprime le contact de l'annuaire
// pour l'afficher dans la barre des destinataires
function getReceiverId(id,firstName,name){
    

    tblReceivers.push(id); // On ajoute l'id du destinataire dans le tableau adéquat
    let receiver = ("<h3  id = '" + `crossIcon${id}`+ "' onclick='getCrossId( " +`${id}`+ ")'>" + " " + "|"+ " " + firstName + " " + name + " "+ "<i  class='fas fa-times'></i>"+"</h3>");  
    
    document.querySelector('.displayList').innerHTML += receiver + " ";
    
    let myDiv = document.querySelector(`#receiver${id}`);
    myDiv.style.display = "none" ;
    
    // alert("Tableau des id_receivers : " + tblReceivers);
        

}



// Fonction qui supprime le contact de la barre des destinataires
// pour le ré-afficher dans l'annuaire
function getCrossId(id){

    effacerHistorique(); // On efface le champ de l'historique
    theIndex = tblReceivers.indexOf(id);
    tblReceivers.splice(theIndex,1);

    
    $(`#crossIcon${id}`).remove(); // Suppression depuis la barre des destinataires du contact
    
    // alert("Tableau des id_receivers : " + tblReceivers);
   

    let myDiv = document.querySelector(`#receiver${id}`);
    myDiv.style.display = "block" ; // R-éaffichage du contact dans l'annuaire        
}



// Fonction qui affiche l'historique des conversations avec
// les destinataires présents dans la barre destinataire
function historique() { 
    
    let id_sender = document.querySelector("#senderId").value; // ID de l'émetteur
    let id_receivers = tblReceivers; // id du destinataire
    let formError = false;
    // Vérification du formulaire
    if(!id_receivers[0]){
        
        $("#no-receiver-modal").modal();
        $("#no-receiver-modal").show();
        formError = true;
    }
    
    if (formError !== true){
        // methode ajax pour envoyer les infos à MessagerieController via la route /messagerie/archive en post
        $.ajax({
        url: "/messagerie/archive",
        type: "post",
        data: {
            id_sender: id_sender,
            id_receivers: id_receivers,
        },
        success: function(data) { // << data >> est la réponse de MessagerieController (elle est au format json)

            let displayedSubject= "";
            
            document.querySelector('#historique').innerHTML = "";
            listeMsg = JSON.parse(data);

            let senderFirstName = "";
            let senderName = "";
            let receiverFirstName = "";
            let receiverName = ""; 


            for(msg of listeMsg){
                console.log(msg.Account_id_sender, " - ", id_sender);
                if(msg.Account_id_sender == id_sender)
                {
                    senderFirstName = "";
                    senderName = "";
                    receiverFirstName = msg.firstName;
                    receiverName = msg.name;
                    bulle = "<li class='clearfix admin_chat' ><div class='chat-body clearfix pull-right'><p style='background-color:#f2d77c'>";       
                }

                else
                {
                    senderFirstName = msg.firstName;
                    senderName = msg.name;     
                    receiverFirstName = "";
                    receiverName = "";
                    bulle = "<li class='clearfix admin_chat' ><div class='chat-body clearfix pull-left'><p style='background-color:#b5c9e8'>";
                }

                // On affiche l'historique des conversation
                document.querySelector('#historique').innerHTML += `${senderFirstName} ${senderName} ${receiverFirstName} ${receiverName} le ${msg.date}<br />Sujet : ${msg.subject} ${bulle} ${msg.texte}</p></div></li>`;
                displayedSubject = msg.subject;
            }
            document.querySelector("#subject-message-input").value = displayedSubject;
            
        },
        error: function() {
            alert("Erreur Ajax !");
        }
    });
    
}
}



// Fonction qui efface le champ historique
function effacerHistorique(){

    document.querySelector('#historique').innerHTML = ""; // On efface le champ de l'historique
    document.querySelector("#subject-message-input").value = ""; // On efface le contenu du champ << sujet >>

           
}



// Fonction qui efface le champ historique
function eraseMsgBar(){

    document.querySelector('#message').value = ""; // On efface le champ de message
    historique();
           
}